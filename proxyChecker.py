import requests
import concurrent.futures


proxylist = []
# proxyFile.txt có định dạng ip:port trên từng dòng (19.18.10.31:4321)
with open('proxyFile.txt', 'r') as f:
    data = f.read()
    proxylist = data.split('\n')

def extract(proxy):
    # Bắt buộc phải có User-Agent
    headers = {'User-Agent': 'abc'}
    try:
        r = requests.get('https://httpbin.org/ip', headers=headers, proxies={'http' : 'http://' + proxy,'https':'https://' +  proxy}, timeout=2)
        print(r.json(), ' | Works')
    except Exception as e:
        # print(e)
        pass
    return proxy

with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.map(extract, proxylist)
